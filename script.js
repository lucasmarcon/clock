function clock() {
    let date = new Date()
    let day = date.getDay()
    let days = ["Sun", "Mon", "Tue", "Wed ", "Thu", "Fri", "Sat"]
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let meridiem = date.getHours() < 12 ? 'AM' : 'PM'
    let seconds = date.getSeconds()

    let auxSeconds = seconds * 6
    let auxMinutes = (minutes * 6) + (auxSeconds / 60)
    let auxHours = ((((hours % 12) / 12) * 360) + (auxMinutes / 12)) + 90 

    $('#seconds').css("transform", "rotate(" + auxSeconds + "deg)");
    $('#minutes').css("transform", "rotate(" + auxMinutes + "deg)");
    $('#hours').css("transform", "rotate(" + auxHours + "deg)");
	
    let hour = (hours == 0) ? 12 : (hours <= 12) ? hours : hours - 12
    let minute = (minutes < 10 ? "0" + minutes : minutes)

    $('#meridiem').text(hour + ":" + minute + " " + meridiem)
    $('#weekday').text(days[day])
}

function hourUpdate() {
    setInterval(() => {
        clock()
    }, 1000)
}

hourUpdate()